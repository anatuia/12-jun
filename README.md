# 12-Jun


El [cadáver exquisito](https://www.educ.ar/recursos/fullscreen/show/24966) era un juego surrealista de creación colectiva, que podía ser escrito o gráfico. En esta obra cada miembro del grupo realizaba su parte de la obra sin conocer por completo las otras partes.


La propuesta es jugar a este juego desde git.


Para empezar, realicen por grupo un fork o bifurcación de este proyecto en sus cuentas.


Luego clonenlo desde la consola.
```
git clone <https_tu_url> <carpeta_destino>
```
y metanse adentro de la carpeta donde se descargó el repositorio
```
cd <carpeta_destino>
```




Para el primer uso deberán autenticarse, y quizá configurar usuario y email, lean las instrucciones del programa git que siempre da pistas de qué hacer.


# Reglas del Cadáver Exquisito
Antes de arrancar, elijan en qué orden van a jugar. Una vez que termine el jugador anterior(haya realizado el commit), puede seguir el siguiente. Pueden realizar tantas rondas como quieran.
1. Descargar la última versión del `cadaver_exquisito.txt`.
```
git pull
```
2. Leer *solamente* la última palabra del archivo.
```
$ rev cadaver_exquisito.txt | cut -d ' ' -f 1 | rev 
tormentosa
```
3. Añadir una línea al archivo inspirándose en la última palabra
```
echo ' y estrepitosamente abrió la puerta, se detuvo a darme una mirada aniquiladora y se marchó sin tan sólo decirme una palabra' >> cadaver_exquisito.txt
```
4. Después de añadir la línea, agregar el archivo `cadaver_exquisito.txt` para que se registren sus cambios (stage area)
```
git add cadaver_exquisito.txt
```
5. Hacer un commit con un mensaje descriptivo.
```
git commit -m "agrego una linea al cadaver exquisito"
```
6. Hacer un push para que las modificaciones esten disponibles para el próximo jugador.
```
git push
```




¡Diviértanse y sean creativos!



